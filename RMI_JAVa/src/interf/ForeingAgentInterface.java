/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interf;

import java.rmi.Remote;
import java.rmi.RemoteException;
import model.Message;

/**
 *
 * @author aluno
 */
public interface ForeingAgentInterface extends Remote{
    public void mostraMensagem(Message message) throws RemoteException;
    public void enviaMensagem(Message message) throws RemoteException;
}
