/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interf;

import model.Message;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author aluno
 */
public interface HomeAgentInterface extends Remote {
    
    public void enviaMensagem(Message message) throws RemoteException;;
    
}
