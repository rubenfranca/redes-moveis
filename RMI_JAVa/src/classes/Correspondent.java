/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import interf.ConstantHomeAgent;
import interf.HomeAgentInterface;
import model.Message;
import java.rmi.Remote; // Para se tornar um Objeto Remoto, a classe deve implementar uma interface remota
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.JOptionPane;

/**
 *
 * @author Guilherme
 */
public class Correspondent implements Remote {

    String ip;
    String ipHomeAgent;

    public Correspondent() {
    }

    public static void main(String[] args) {

        try {
            Message message = new Message();
            String ipHomeAgent = "172.16.0.101";
            String ipCorrespondent = "172.16.0.102";

            Registry registry = LocateRegistry.getRegistry(ipHomeAgent, ConstantHomeAgent.RMI_PORT);
            final HomeAgentInterface homeAgentInterface = (HomeAgentInterface) registry.lookup(ConstantHomeAgent.RMI_ID);

            String conteudoMensagem = JOptionPane.showInputDialog("Digite sua mensagem:");

            message.setConteudo(conteudoMensagem);
            message.setIpDestinatario("172.16.0.98");
            message.setIpRemetente(ipCorrespondent);

            homeAgentInterface.enviaMensagem(message);

        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
