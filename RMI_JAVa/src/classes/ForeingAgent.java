/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import interf.ConstantForeingAgent;
import interf.ConstantMobileAgent;
import interf.HomeAgentInterface;
import model.Message;
import java.lang.reflect.Array;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import javax.swing.JOptionPane;
import interf.ForeingAgentInterface;
import interf.MobileAgentInterface;
import java.rmi.RemoteException;

/**
 *
 * @author Rúben
 */
public class ForeingAgent implements ForeingAgentInterface {

    String ip = "172.16.0.99";
    Message mensagem;
    boolean resultado;

    public String getIp() {
        return ip;
    }

    public Message getMensagem() {
        return mensagem;
    }

    public boolean checkRoutTable(Message mesagem) {

        return false;
    }

    public void sendMessage(Array ipCareOfAddres) {

    }

    
    public boolean checkRouteTable(Message mensagem) {
        resultado = false;
//        ArrayList destinatario = new ArrayList();
//        destinatario = RouteTable.getDestinatario();
//
//        for (int i = 0; i <= destinatario.size(); i++) {
//
//            if (mensagem.getIpDestinatario().equals(destinatario.get(i))) {
//                resultado = true;
//            }
//        }
//
//        if (!resultado) {
//            this.ipCareOfAdress = (String) RouteTable.getCareOfAdress().get(0);
//        }

        if (mensagem.getIpDestinatario().equals(this.ip)) {
            mostraMensagem(mensagem);
        } else {
            this.cliente(mensagem);
        }

        return resultado;
    }

    public static void main(String args[]) {

        try {

            ForeingAgent foreingAgent = new ForeingAgent();
            ForeingAgentInterface foreingAgentInterface = (ForeingAgentInterface) UnicastRemoteObject.exportObject(foreingAgent, 0);

            Registry registry = LocateRegistry.createRegistry(ConstantForeingAgent.RMI_PORT);

            registry.bind(ConstantForeingAgent.RMI_ID, foreingAgentInterface);

            System.out.println("Foreing Agent ready!");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }

    }

  
    public void enviaMensagem(Message message) throws RemoteException {
//        JOptionPane.showMessageDialog(null, message.toString());
//        this.setMensagem(message);
        System.out.println("PASSEI AQUI NESSA MERDA");
        this.checkRouteTable(message);
    }

    public void mostraMensagem(Message message) {
        JOptionPane.showMessageDialog(null, message.toString());
    }

    public void cliente(Message message) {
        try {
            String ipMobileAgent = "172.16.0.98";

            Registry registry = LocateRegistry.getRegistry(ipMobileAgent, ConstantMobileAgent.RMI_PORT);
            final MobileAgentInterface mobileAgentInterface = (MobileAgentInterface) registry.lookup(ConstantMobileAgent.RMI_ID);

            mobileAgentInterface.mostraMensagem(message);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
