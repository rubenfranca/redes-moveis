package model;

import java.io.Serializable;

import java.rmi.Remote; // Para se tornar um Objeto Remoto, a classe deve implementar uma interface remota

/**
 *
 * @author Guilherme
 */
public class Message implements Serializable {

    String conteudo;
    String ipDestinatario;
    String ipRemetente;

    public Message(String conteudo, String ipDestinatario) {
        this.conteudo = conteudo;
        this.ipDestinatario = ipDestinatario;
    }

    public Message() {
    }
    
    private static final long serialVersionUID = 80L;
    
    

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public String getIpDestinatario() {
        return ipDestinatario;
    }

    public void setIpDestinatario(String ipDestinatario) {
        this.ipDestinatario = ipDestinatario;
    }
    
    public void gravaMensagem(String mensagem){
        this.setConteudo(mensagem);
    }

    public String getIpRemetente() {
        return ipRemetente;
    }

    public void setIpRemetente(String ipRemetente) {
        this.ipRemetente = ipRemetente;
    }

    @Override
    public String toString() {
        return conteudo;
    }
    
    
    

}
