/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.lang.reflect.Array;
import java.rmi.Remote; // Para se tornar um Objeto Remoto, a classe deve implementar uma interface remota
import java.util.ArrayList;

/**
 *
 * @author Rúben
 */
public class RouteTable implements Remote {
    
    private ArrayList<String> destinatario = new ArrayList<String>();
    private ArrayList<String> careOfAdress = new ArrayList<String>();

    public RouteTable() {
    }

    public String getDestinatario() {
        return destinatario.get(0);
    }

    public void setDestinatario(String destinatario) {
        this.destinatario.add(destinatario);
    }

    public String getCareOfAdress() {
        return careOfAdress.get(0);
    }

    public void setCareOfAdress(String careOfAdress) {
        this.careOfAdress.add(careOfAdress);
    }

    
    
    
}
