/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import model.RouteTable;
import model.Message;
import interf.ConstantHomeAgent;
import interf.ConstantForeingAgent;
import interf.HomeAgentInterface;
import interf.ForeingAgentInterface;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.rmi.Remote; // Para se tornar um Objeto Remoto, a classe deve implementar uma interface remota
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Guilherme
 */
public class HomeAgent implements HomeAgentInterface {

    private Message mensagem;
    private final String ip = "172.16.0.101";
    private final String ipCareOfAdress = "172.16.0.92";
    boolean resultado;
    RouteTable routeTable;

    public HomeAgent() {
        this.criaRouteTable();
    }

    public Message getMensagem() {
        return mensagem;
    }

    public void setMensagem(Message mensagem) {
        this.mensagem = mensagem;
    }

    public String getIp() {
        return ip;
    }

    public boolean checkRouteTable(Message mensagem) {
        resultado = false;
        if (mensagem.getIpDestinatario().equals(this.routeTable.getDestinatario())) {
            mostraMensagem(mensagem);
        } else {
            this.cliente(mensagem);
        }

        return resultado;
    }

    public static void main(String args[]) {

        try {

            HomeAgent homeAgent = new HomeAgent();
            HomeAgentInterface homeAgentInterface = (HomeAgentInterface) UnicastRemoteObject.exportObject(homeAgent, 0);

            Registry registry = LocateRegistry.createRegistry(ConstantHomeAgent.RMI_PORT);

            registry.bind(ConstantHomeAgent.RMI_ID, homeAgentInterface);

            System.out.println("Home Agent ready!");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }

    }

    @Override
    public void enviaMensagem(Message message) throws RemoteException {
//        JOptionPane.showMessageDialog(null, message.toString());
//        this.setMensagem(message);
        System.out.println("PASSEI AQUI SEUS BOSTA");
        this.checkRouteTable(message);
    }

    public void mostraMensagem(Message message) {
        JOptionPane.showMessageDialog(null, message.toString());
    }

    public void cliente(Message message) {
        try {
//            String ipForeingAgent = "172.16.0.92";

            Registry registry = LocateRegistry.getRegistry(this.routeTable.getCareOfAdress(), ConstantForeingAgent.RMI_PORT);
            final ForeingAgentInterface foreingAgentInterface = (ForeingAgentInterface) registry.lookup(ConstantForeingAgent.RMI_ID);

            foreingAgentInterface.mostraMensagem(message);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

    private void criaRouteTable() {
        routeTable = new RouteTable();

        routeTable.setDestinatario(this.ip);
        routeTable.setCareOfAdress(this.ipCareOfAdress);
    }
}
