/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;
import interf.ConstantForeingAgent;
import interf.ConstantMobileAgent;
import interf.ForeingAgentInterface;
import interf.MobileAgentInterface;
import model.Message;
import java.rmi.Remote; // Para se tornar um Objeto Remoto, a classe deve implementar uma interface remota
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import javax.swing.JOptionPane;
/**
 *
 * @author Bruno
 */
public class MobileAgent implements MobileAgentInterface {
    private String ip = "172.16.0.98";
    private Message mensagem;
    boolean resultado;

    public MobileAgent() {
    }
    
    public String getIp() {
        return ip;
    }

    public Message getMensagem() {
        return mensagem;
    }
    
    public boolean checkRouteTable(Message mensagem) {
        resultado = false;
//        ArrayList destinatario = new ArrayList();
//        destinatario = RouteTable.getDestinatario();
//
//        for (int i = 0; i <= destinatario.size(); i++) {
//
//            if (mensagem.getIpDestinatario().equals(destinatario.get(i))) {
//                resultado = true;
//            }
//        }
//
//        if (!resultado) {
//            this.ipCareOfAdress = (String) RouteTable.getCareOfAdress().get(0);
//        }

        if (mensagem.getIpDestinatario().equals(this.ip)) {
            mostraMensagem(mensagem);
        } else {
           
        }

        return resultado;
    }
    
    public static void main(String args[]) {

        try {

            MobileAgent mobileAgent = new MobileAgent();
            MobileAgentInterface mobileAgentInterface = (MobileAgentInterface) UnicastRemoteObject.exportObject(mobileAgent, 0);

            Registry registry = LocateRegistry.createRegistry(ConstantMobileAgent.RMI_PORT);

            registry.bind(ConstantMobileAgent.RMI_ID, mobileAgentInterface);

            System.out.println("Mobile Agent ready!");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }

    }
    
    public void mostraMensagem(Message message) {
        JOptionPane.showMessageDialog(null, message.toString());
    }
    
}
