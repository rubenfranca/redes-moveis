package client;

import interf.ConstantHomeAgent;
import interf.HelloInterface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIClient {

	private RMIClient() {
	}

	public static void main(String[] args) {
		
		try {
			Registry registry  = LocateRegistry.getRegistry("localhost",ConstantHomeAgent.RMI_PORT);
			final HelloInterface remote = (HelloInterface) registry.lookup(ConstantHomeAgent.RMI_ID);
			String response = remote.sayHello();
			System.out.println("Response: " + response);
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
	